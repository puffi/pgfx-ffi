#pragma once

#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>


struct _pgfx;
typedef struct _pgfx* pgfx_gfx_t;


typedef enum {
	PGFX_PLATFORM_TYPE_NONE,
	PGFX_PLATFORM_TYPE_WAYLAND,
	PGFX_PLATFORM_TYPE_XLIB,
	PGFX_PLATFORM_TYPE_WIN32,
} pgfx_platform_type_t;

typedef struct {
	void* surface;
	void* display;
	void* egl_window;
} pgfx_platform_wayland_t;

typedef struct {
	uint64_t window;
	void* display;
} pgfx_platform_xlib_t;

typedef struct {
	void* hwnd;
	void* hdc;
	void* hinstance;
} pgfx_platform_win32_t;

typedef struct {
	pgfx_platform_type_t type;
	union {
		pgfx_platform_wayland_t wayland;
		pgfx_platform_xlib_t xlib;
		pgfx_platform_win32_t win32;
	};
} pgfx_platform_handle_t;

typedef struct {
	uintptr_t max_vertex_buffers;
	uintptr_t max_index_buffers;
	uintptr_t max_attributes;
	uintptr_t vertex_buffer_size;
	uintptr_t max_vertex_shaders;
	uintptr_t max_fragment_shaders;
	uintptr_t max_render_targets;
	uintptr_t max_textures;
	uintptr_t max_bound_textures;
	uintptr_t max_samplers;
	uintptr_t max_uniform_buffers;
	uintptr_t max_bound_uniform_buffers;
	uintptr_t max_uniform_buffer_size;
	uintptr_t max_storage_buffers;
	uintptr_t max_bound_storage_buffers;
	uintptr_t max_storage_buffer_size;
	uintptr_t staging_buffer_size;
} pgfx_config_t;

pgfx_config_t pgfx_config_init();

// handles //
#define PGFX_HANDLE(name, id_type) \
	typedef struct \
{ \
	id_type id; \
} pgfx_##name##_handle_t; \
pgfx_##name##_handle_t pgfx_##name##_handle_init(); \
pgfx_##name##_handle_t pgfx_##name##_handle_invalid(); \
bool pgfx_##name##_handle_is_valid(pgfx_##name##_handle_t handle); \
id_type pgfx_##name##_handle_get_id(pgfx_##name##_handle_t handle); \

#define PGFX_RAW_HANDLE(name, id_type) \
	typedef struct \
{ \
	id_type id; \
} pgfx_##name##_handle_t; \
pgfx_##name##_handle_t pgfx_##name##_handle_init(); \
pgfx_##name##_handle_t pgfx_##name##_handle_invalid(); \
bool pgfx_##name##_handle_is_valid(pgfx_##name##_handle_t handle); \
id_type pgfx_##name##_handle_get_raw_handle(pgfx_##name##_handle_t handle); \


PGFX_HANDLE(render_target, uint16_t);
PGFX_HANDLE(texture, uint16_t);
PGFX_HANDLE(vertex_shader, uint16_t);
PGFX_HANDLE(fragment_shader, uint16_t);
PGFX_HANDLE(vertex_buffer, uint16_t);
PGFX_HANDLE(index_buffer, uint16_t);
PGFX_HANDLE(uniform_buffer, uint16_t);
PGFX_HANDLE(storage_buffer, uint16_t);

PGFX_RAW_HANDLE(bindless_texture, size_t);

pgfx_render_target_handle_t pgfx_render_target_default();

// present //
typedef enum {
	PGFX_PRESENT_MODE_VSYNC,
	PGFX_PRESENT_MODE_IMMEDIATE,
} pgfx_present_mode_t;

// clear //
typedef enum {
	PGFX_CLEAR_VALUE_NONE,
	PGFX_CLEAR_VALUE_F32,
	PGFX_CLEAR_VALUE_I32,
	PGFX_CLEAR_VALUE_U32,
	PGFX_CLEAR_VALUE_VEC4,
	PGFX_CLEAR_VALUE_F32I32,
} pgfx_clear_value_type_t;

typedef struct {
	float f32;
	int32_t i32;
} pgfx_clear_value_f32i32_t;

typedef struct {
	pgfx_clear_value_type_t type;
	union {
		float f32;
		int32_t i32;
		uint32_t u32;
		float vec4[4];
		pgfx_clear_value_f32i32_t f32i32;
	};
} pgfx_clear_value_t;

// viewport //
typedef struct {
	int32_t x;
	int32_t y;
	int32_t width;
	int32_t height;
} pgfx_viewport_t;


// render target //
typedef enum {
	PGFX_ATTACHMENT_COLOR_0,
	PGFX_ATTACHMENT_COLOR_1,
	PGFX_ATTACHMENT_COLOR_2,
	PGFX_ATTACHMENT_COLOR_3,
	PGFX_ATTACHMENT_COLOR_4,
	PGFX_ATTACHMENT_COLOR_5,
	PGFX_ATTACHMENT_COLOR_6,
	PGFX_ATTACHMENT_COLOR_7,
	PGFX_ATTACHMENT_DEPTH,
	PGFX_ATTACHMENT_STENCIL,
	PGFX_ATTACHMENT_DEPTH_STENCIL,
} pgfx_attachment_t;


// vertex/index //

typedef enum {
	PGFX_INDEX_TYPE_U16,
	PGFX_INDEX_TYPE_U32,
} pgfx_index_type_t;

typedef enum {
	PGFX_ATTRIBUTE_TYPE_NONE,
	PGFX_ATTRIBUTE_TYPE_U8,
	PGFX_ATTRIBUTE_TYPE_F32,
	PGFX_ATTRIBUTE_TYPE_I32,
	PGFX_ATTRIBUTE_TYPE_U32,
} pgfx_attribute_type_t;

typedef struct {
	uint8_t location;
	uint8_t type; // needs to be 1 byte for correct alignment/size of struct
	uint8_t num;
	bool normalize;
} pgfx_attribute_t;


// texture //

typedef enum {
	PGFX_FORMAT_R8,
	PGFX_FORMAT_RGB8,
	PGFX_FORMAT_RGBA8,
	PGFX_FORMAT_RGB16F,
	PGFX_FORMAT_RGBA16F,
	PGFX_FORMAT_RGB32F,
	PGFX_FORMAT_RGBA32F,
	PGFX_FORMAT_DEPTH32,
	PGFX_FORMAT_DEPTH32_STENCIL8,
} pgfx_format_t;

typedef enum {
	PGFX_FILTER_NEAREST,
	PGFX_FILTER_LINEAR,
} pgfx_filter_t;

typedef enum {
	PGFX_WRAP_REPEAT,
	PGFX_WRAP_MIRRORED_REPEAT,
	PGFX_WRAP_CLAMP,
} pgfx_wrap_t;

typedef enum {
	PGFX_ANISOTROPIC_FILTER_NONE,
	PGFX_ANISOTROPIC_FILTER_X4,
	PGFX_ANISOTROPIC_FILTER_X8,
	PGFX_ANISOTROPIC_FILTER_X16,
} pgfx_anisotropic_filter_t;

typedef enum {
	PGFX_TEXTURE_TYPE_2D,
} pgfx_texture_type_t;

typedef struct {
	uint16_t _bits;
} pgfx_sampler_t;


pgfx_sampler_t pgfx_sampler_init();
void pgfx_sampler_set_filter(pgfx_sampler_t* sampler, pgfx_filter_t filter);
void pgfx_sampler_set_min_filter(pgfx_sampler_t* sampler, pgfx_filter_t filter);
void pgfx_sampler_set_mag_filter(pgfx_sampler_t* sampler, pgfx_filter_t filter);
void pgfx_sampler_set_anisotropic_filter(pgfx_sampler_t* sampler, pgfx_anisotropic_filter_t anisotropic);
void pgfx_sampler_set_wrap(pgfx_sampler_t* sampler, pgfx_wrap_t wrap);
void pgfx_sampler_set_wrap_u(pgfx_sampler_t* sampler, pgfx_wrap_t wrap);
void pgfx_sampler_set_wrap_v(pgfx_sampler_t* sampler, pgfx_wrap_t wrap);
void pgfx_sampler_set_wrap_w(pgfx_sampler_t* sampler, pgfx_wrap_t wrap);
void pgfx_sampler_get_hash(pgfx_sampler_t* sampler);
void pgfx_sampler_get_filter(pgfx_sampler_t* sampler);
void pgfx_sampler_get_min_filter(pgfx_sampler_t* sampler);
void pgfx_sampler_get_mag_filter(pgfx_sampler_t* sampler);
void pgfx_sampler_get_anisotropic_filter(pgfx_sampler_t* sampler);
void pgfx_sampler_get_wrap(pgfx_sampler_t* sampler);
void pgfx_sampler_get_wrap_u(pgfx_sampler_t* sampler);
void pgfx_sampler_get_wrap_v(pgfx_sampler_t* sampler);
void pgfx_sampler_get_wrap_w(pgfx_sampler_t* sampler);


// draw state //

typedef enum {
	PGFX_BLEND_FACTOR_ZERO,
	PGFX_BLEND_FACTOR_ONE,
	PGFX_BLEND_FACTOR_SRC_COLOR,
	PGFX_BLEND_FACTOR_ONE_MINUS_SRC_COLOR,
	PGFX_BLEND_FACTOR_DST_COLOR,
	PGFX_BLEND_FACTOR_ONE_MINUS_DST_COLOR,
	PGFX_BLEND_FACTOR_SRC_ALPHA,
	PGFX_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
	PGFX_BLEND_FACTOR_DST_ALPHA,
	PGFX_BLEND_FACTOR_ONE_MINUS_DST_ALPHA,
	PGFX_BLEND_FACTOR_CONSTANT_COLOR,
	PGFX_BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR,
	PGFX_BLEND_FACTOR_CONSTANT_ALPHA,
	PGFX_BLEND_FACTOR_ONE_MINUS_CONSTANT_ALPHA,
	PGFX_BLEND_FACTOR_SRC_ALPHA_SATURATE,
	PGFX_BLEND_FACTOR_SRC1_COLOR,
	PGFX_BLEND_FACTOR_ONE_MINUS_SRC1_COLOR,
	PGFX_BLEND_FACTOR_ONE_MINUS_SRC1_ALPHA,
} pgfx_blend_factor_t;

/**
 * default src: ONE, dest: ZERO
 */
typedef struct {
	pgfx_blend_factor_t source;
	pgfx_blend_factor_t destination;
} pgfx_blend_func_t;

typedef enum {
	PGFX_BLEND_EQUATION_ADD,
	PGFX_BLEND_EQUATION_SUBTRACT,
	PGFX_BLEND_EQUATION_REVERSE_SUBTRACT,
	PGFX_BLEND_EQUATION_MIN,
	PGFX_BLEND_EQUATION_MAX,
} pgfx_blend_equation_t;

typedef struct {
	pgfx_blend_func_t func;
	pgfx_blend_equation_t equation;
} pgfx_blend_state_t;

// default == less
typedef enum {
	PGFX_DEPTH_FUNC_NEVER,
	PGFX_DEPTH_FUNC_LESS,
	PGFX_DEPTH_FUNC_EQUAL,
	PGFX_DEPTH_FUNC_LEQUAL,
	PGFX_DEPTH_FUNC_GREATER,
	PGFX_DEPTH_FUNC_NOT_EQUAL,
	PGFX_DEPTH_FUNC_GEQUAL,
	PGFX_DEPTH_FUNC_ALWAYS,
} pgfx_depth_func_t;

typedef struct {
	pgfx_depth_func_t func;
} pgfx_depth_state_t;

typedef enum {
	PGFX_CULL_MODE_FRONT,
	PGFX_CULL_MODE_BACK,
	PGFX_CULL_MODE_FRONT_AND_BACK,
} pgfx_cull_mode_t;

typedef enum {
	PGFX_CULL_FRONT_FACE_CW,
	PGFX_CULL_FRONT_FACE_CCW,
} pgfx_cull_front_face_t;

typedef struct {
	pgfx_cull_mode_t mode;
	pgfx_cull_front_face_t front_face;
} pgfx_cull_state_t;

typedef struct {
	int32_t rect[4];
} pgfx_scissor_state_t;


// utils //
#define TYPED_SLICE(name, type) \
	typedef struct { \
		type* payload; \
		uintptr_t length; \
	} pgfx_##name##_slice_t;

TYPED_SLICE(u8, uint8_t);

typedef struct {
	pgfx_attachment_t att;
	pgfx_texture_handle_t tex;
	pgfx_clear_value_t cv;
} pgfx_att_tex_cv_t;

TYPED_SLICE(att_tex_cv, pgfx_att_tex_cv_t);
TYPED_SLICE(attribute, pgfx_attribute_t);
