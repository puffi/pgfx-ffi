#pragma once

#include <pgfx/types.h>


/// gfx functions ///

// init/destroy //
pgfx_gfx_t pgfx_gfx_create(pgfx_platform_handle_t platform_handle);
pgfx_gfx_t pgfx_gfx_create_with_config(pgfx_platform_handle_t platform_handle, pgfx_config_t config);

void pgfx_gfx_destroy(pgfx_gfx_t gfx);

void pgfx_init_logging();

// present/draw //
void pgfx_gfx_present(pgfx_gfx_t gfx, pgfx_present_mode_t mode);
void pgfx_gfx_draw(pgfx_gfx_t gfx);
void pgfx_gfx_draw_instances_base(pgfx_gfx_t gfx, uint32_t instances, uint32_t base_instance);



// render target //
void pgfx_gfx_resize_window(pgfx_gfx_t gfx, uint32_t size_x, uint32_t size_y);
pgfx_render_target_handle_t pgfx_gfx_create_render_target(pgfx_gfx_t gfx, pgfx_att_tex_cv_slice_t textures);
void pgfx_gfx_destroy_render_target(pgfx_gfx_t gfx, pgfx_render_target_handle_t handle);

// clear //
void pgfx_gfx_clear_render_target(pgfx_gfx_t gfx, pgfx_render_target_handle_t handle);
void pgfx_gfx_clear_render_target_att_cv(pgfx_gfx_t gfx, pgfx_render_target_handle_t handle, pgfx_attachment_t att, pgfx_clear_value_t cv);

// shader //
pgfx_vertex_shader_handle_t pgfx_gfx_create_vertex_shader(pgfx_gfx_t gfx, pgfx_u8_slice_t data);
void pgfx_gfx_destroy_vertex_shader(pgfx_gfx_t gfx, pgfx_vertex_shader_handle_t handle);
pgfx_fragment_shader_handle_t pgfx_gfx_create_fragment_shader(pgfx_gfx_t gfx, pgfx_u8_slice_t data);
void pgfx_gfx_destroy_index_shader(pgfx_gfx_t gfx, pgfx_fragment_shader_handle_t handle);

// vertex/index buffer //
pgfx_vertex_buffer_handle_t pgfx_gfx_create_vertex_buffer(pgfx_gfx_t gfx, pgfx_u8_slice_t data, pgfx_attribute_slice_t attributes);
void pgfx_gfx_destroy_vertex_buffer(pgfx_gfx_t gfx, pgfx_vertex_buffer_handle_t handle);
pgfx_index_buffer_handle_t pgfx_gfx_create_index_buffer(pgfx_gfx_t gfx, pgfx_u8_slice_t data, pgfx_index_type_t index_type);
void pgfx_gfx_destroy_index_buffer(pgfx_gfx_t gfx, pgfx_index_buffer_handle_t handle);

// uniform/storage buffer //
pgfx_uniform_buffer_handle_t pgfx_gfx_create_uniform_buffer(pgfx_gfx_t gfx, uint32_t size);
void pgfx_gfx_destroy_uniform_buffer(pgfx_gfx_t gfx, pgfx_uniform_buffer_handle_t handle);
void pgfx_gfx_update_uniform_buffer(pgfx_gfx_t gfx, pgfx_uniform_buffer_handle_t handle, pgfx_u8_slice_t data);
void pgfx_gfx_update_uniform_buffer_offset(pgfx_gfx_t gfx, pgfx_uniform_buffer_handle_t handle, pgfx_u8_slice_t data, uint32_t offset);
pgfx_storage_buffer_handle_t pgfx_gfx_create_storage_buffer(pgfx_gfx_t gfx, uint32_t size);
void pgfx_gfx_destroy_storage_buffer(pgfx_gfx_t gfx, pgfx_storage_buffer_handle_t handle);
void pgfx_gfx_update_storage_buffer(pgfx_gfx_t gfx, pgfx_storage_buffer_handle_t handle, pgfx_u8_slice_t data);
void pgfx_gfx_update_storage_buffer_offset(pgfx_gfx_t gfx, pgfx_storage_buffer_handle_t handle, pgfx_u8_slice_t data, uint32_t offset);

// texture/sampler //
void pgfx_gfx_destroy_texture(pgfx_gfx_t gfx, pgfx_texture_handle_t handle);
pgfx_texture_handle_t pgfx_gfx_create_texture_2d(pgfx_gfx_t gfx, uint32_t width, uint32_t height, pgfx_format_t fmt, uint32_t mip_levels);
void pgfx_gfx_update_texture(pgfx_gfx_t gfx, pgfx_texture_handle_t handle, uint32_t mip_level, uint32_t offset_x, uint32_t offset_y, uint32_t offset_z, uint32_t width, uint32_t height, uint32_t depth, pgfx_u8_slice_t data);
pgfx_bindless_texture_handle_t pgfx_gfx_get_bindless_texture_handle(pgfx_gfx_t gfx, pgfx_texture_handle_t tex, pgfx_sampler_t sampler);

// draw state //
void pgfx_gfx_set_vertex_buffer(pgfx_gfx_t gfx, pgfx_vertex_buffer_handle_t handle);
void pgfx_gfx_set_index_buffer(pgfx_gfx_t gfx, pgfx_index_buffer_handle_t handle);
void pgfx_gfx_set_vertex_shader(pgfx_gfx_t gfx, pgfx_vertex_shader_handle_t handle);
void pgfx_gfx_set_fragment_shader(pgfx_gfx_t gfx, pgfx_fragment_shader_handle_t handle);
void pgfx_gfx_set_texture(pgfx_gfx_t gfx, pgfx_texture_handle_t handle, uint32_t location);
void pgfx_gfx_set_sampler(pgfx_gfx_t gfx, pgfx_sampler_t sampler, uint32_t location);
void pgfx_gfx_set_texture_sampler(pgfx_gfx_t gfx, pgfx_texture_handle_t tex, pgfx_sampler_t sampler, uint32_t location);
void pgfx_gfx_set_uniform_buffer(pgfx_gfx_t gfx, pgfx_uniform_buffer_handle_t handle);
void pgfx_gfx_set_uniform_buffer_offset_size(pgfx_gfx_t gfx, pgfx_uniform_buffer_handle_t handle, uint32_t offset, uint32_t size);
void pgfx_gfx_set_storage_buffer(pgfx_gfx_t gfx, pgfx_storage_buffer_handle_t handle);
void pgfx_gfx_set_storage_buffer_offset_size(pgfx_gfx_t gfx, pgfx_storage_buffer_handle_t handle, uint32_t offset, uint32_t size);
void pgfx_gfx_set_render_targets(pgfx_gfx_t gfx, pgfx_render_target_handle_t read, pgfx_render_target_handle_t write);

void pgfx_gfx_set_blend_state(pgfx_gfx_t gfx, const pgfx_blend_state_t* blend_state);
void pgfx_gfx_set_depth_state(pgfx_gfx_t gfx, const pgfx_depth_state_t* depth_state);
void pgfx_gfx_set_cull_state(pgfx_gfx_t gfx, const pgfx_cull_state_t* cull_state);
void pgfx_gfx_set_scissor_state(pgfx_gfx_t gfx, const pgfx_scissor_state_t* scissor_state);
// viewport //
void pgfx_gfx_set_viewport(pgfx_gfx_t gfx, pgfx_viewport_t viewport);

