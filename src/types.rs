use std::{
    ffi::{c_ulong, c_void},
    fmt::Debug,
};

use crate::{pgfx_define_handle, pgfx_define_raw_handle};

pub struct _pgfx_gfx;
pub type pgfx_gfx_t = *mut _pgfx_gfx;

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub enum pgfx_platform_type_t {
    None,
    Wayland,
    Xlib,
    Win32,
}

// workaround for using the ffi bindings in rust
// don't expose in the C header
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct pgfx_platform_none_t(pub u8);

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct pgfx_platform_wayland_t {
    pub surface: *mut c_void,
    pub display: *mut c_void,
    pub egl_window: *mut c_void,
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct pgfx_platform_xlib_t {
    pub window: c_ulong,
    pub display: *mut c_void,
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct pgfx_platform_win32_t {
    pub hwnd: *mut c_void,
    pub hdc: *mut c_void,
    pub hinstance: *mut c_void,
}

#[repr(C)]
#[derive(Copy, Clone)]
pub union pgfx_platform_union_t {
    pub none: pgfx_platform_none_t,
    pub wayland: pgfx_platform_wayland_t,
    pub xlib: pgfx_platform_xlib_t,
    pub win32: pgfx_platform_win32_t,
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct pgfx_platform_handle_t {
    pub ty: pgfx_platform_type_t,
    pub handles: pgfx_platform_union_t,
}

impl Debug for pgfx_platform_handle_t {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut debug_struct = f.debug_struct("pgfx_platform_handle_t");
        let temp = debug_struct.field("type", &self.ty);
        match self.ty {
            pgfx_platform_type_t::None => temp.finish(),
            pgfx_platform_type_t::Wayland => temp
                .field("wayland", unsafe { &self.handles.wayland })
                .finish(),
            pgfx_platform_type_t::Xlib => {
                temp.field("xlib", unsafe { &self.handles.xlib }).finish()
            }
            pgfx_platform_type_t::Win32 => {
                temp.field("win32", unsafe { &self.handles.win32 }).finish()
            }
        }
    }
}

impl From<pgfx_platform_handle_t> for pgfx::common::PlatformHandle {
    fn from(value: pgfx_platform_handle_t) -> Self {
        match value.ty {
            pgfx_platform_type_t::None => pgfx::common::PlatformHandle::None,
            pgfx_platform_type_t::Wayland => {
                let (surface, display, egl_window) = unsafe {
                    (
                        value.handles.wayland.surface,
                        value.handles.wayland.display,
                        value.handles.wayland.egl_window,
                    )
                };
                pgfx::common::PlatformHandle::Wayland {
                    surface,
                    display,
                    egl_window,
                }
            }
            pgfx_platform_type_t::Xlib => {
                let (window, display) =
                    unsafe { (value.handles.xlib.window, value.handles.xlib.display) };
                pgfx::common::PlatformHandle::Xlib { window, display }
            }
            pgfx_platform_type_t::Win32 => {
                let (hwnd, hdc, hinstance) = unsafe {
                    (
                        value.handles.win32.hwnd,
                        value.handles.win32.hdc,
                        value.handles.win32.hinstance,
                    )
                };
                pgfx::common::PlatformHandle::Win32 {
                    hwnd,
                    hdc,
                    hinstance,
                }
            }
        }
    }
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct pgfx_config_t {
    pub max_vertex_buffers: usize,
    pub max_index_buffers: usize,
    pub max_attributes: usize,
    pub vertex_buffer_size: usize,
    pub max_vertex_shaders: usize,
    pub max_fragment_shaders: usize,
    pub max_render_targets: usize,
    pub max_textures: usize,
    pub max_bound_textures: usize,
    pub max_samplers: usize,
    pub max_uniform_buffers: usize,
    pub max_bound_uniform_buffers: usize,
    pub max_uniform_buffer_size: usize,
    pub max_storage_buffers: usize,
    pub max_bound_storage_buffers: usize,
    pub max_storage_buffer_size: usize,
    pub staging_buffer_size: usize,
}

impl From<pgfx_config_t> for pgfx::config::Config {
    fn from(value: pgfx_config_t) -> Self {
        Self {
            max_vertex_buffers: value.max_vertex_buffers,
            max_index_buffers: value.max_index_buffers,
            max_attributes: value.max_attributes,
            vertex_buffer_size: value.vertex_buffer_size,
            max_vertex_shaders: value.max_vertex_shaders,
            max_fragment_shaders: value.max_fragment_shaders,
            max_render_targets: value.max_render_targets,
            max_textures: value.max_textures,
            max_bound_textures: value.max_bound_textures,
            max_samplers: value.max_samplers,
            max_uniform_buffers: value.max_uniform_buffers,
            max_bound_uniform_buffers: value.max_bound_uniform_buffers,
            max_uniform_buffer_size: value.max_uniform_buffer_size,
            max_storage_buffers: value.max_storage_buffers,
            max_bound_storage_buffers: value.max_bound_storage_buffers,
            max_storage_buffer_size: value.max_storage_buffer_size,
            staging_buffer_size: value.staging_buffer_size,
        }
    }
}

impl From<pgfx::config::Config> for pgfx_config_t {
    fn from(value: pgfx::config::Config) -> Self {
        Self {
            max_vertex_buffers: value.max_vertex_buffers,
            max_index_buffers: value.max_index_buffers,
            max_attributes: value.max_attributes,
            vertex_buffer_size: value.vertex_buffer_size,
            max_vertex_shaders: value.max_vertex_shaders,
            max_fragment_shaders: value.max_fragment_shaders,
            max_render_targets: value.max_render_targets,
            max_textures: value.max_textures,
            max_bound_textures: value.max_bound_textures,
            max_samplers: value.max_samplers,
            max_uniform_buffers: value.max_uniform_buffers,
            max_bound_uniform_buffers: value.max_bound_uniform_buffers,
            max_uniform_buffer_size: value.max_uniform_buffer_size,
            max_storage_buffers: value.max_storage_buffers,
            max_bound_storage_buffers: value.max_bound_storage_buffers,
            max_storage_buffer_size: value.max_storage_buffer_size,
            staging_buffer_size: value.staging_buffer_size,
        }
    }
}

// handles //

pgfx_define_handle!(render_target, u16, pgfx::common::RenderTargetHandle);
pgfx_define_handle!(texture, u16, pgfx::common::TextureHandle);
pgfx_define_handle!(vertex_shader, u16, pgfx::common::VertexShaderHandle);
pgfx_define_handle!(fragment_shader, u16, pgfx::common::FragmentShaderHandle);
pgfx_define_handle!(uniform_buffer, u16, pgfx::common::UniformBufferHandle);
pgfx_define_handle!(storage_buffer, u16, pgfx::common::StorageBufferHandle);
pgfx_define_handle!(vertex_buffer, u16, pgfx::common::VertexBufferHandle);
pgfx_define_handle!(index_buffer, u16, pgfx::common::IndexBufferHandle);

// raw handle
pgfx_define_raw_handle!(bindless_texture, usize, pgfx::common::BindlessTextureHandle);

const DEFAULT_RENDER_TARGET: pgfx_render_target_handle_t = pgfx_render_target_handle_t {
    id: pgfx::config::DEFAULT_RENDER_TARGET_INDEX() as _,
};

#[no_mangle]
pub extern "C" fn pgfx_config_init() -> pgfx_config_t {
    pgfx::config::Config::default().into()
}

#[no_mangle]
pub extern "C" fn pgfx_render_target_default() -> pgfx_render_target_handle_t {
    DEFAULT_RENDER_TARGET
}

// present //

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub enum pgfx_present_mode_t {
    Vsync,
    Immediate,
}

impl From<pgfx_present_mode_t> for pgfx::common::PresentMode {
    fn from(value: pgfx_present_mode_t) -> Self {
        match value {
            pgfx_present_mode_t::Vsync => pgfx::common::PresentMode::Vsync,
            pgfx_present_mode_t::Immediate => pgfx::common::PresentMode::Immediate,
        }
    }
}

// viewport //
#[repr(C)]
pub struct pgfx_viewport_t {
    pub x: i32,
    pub y: i32,
    pub width: i32,
    pub height: i32,
}

impl From<pgfx_viewport_t> for pgfx::common::Viewport {
    fn from(value: pgfx_viewport_t) -> Self {
        Self {
            x: value.x,
            y: value.y,
            width: value.width,
            height: value.height,
        }
    }
}

// render target
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub enum pgfx_attachment_t {
    Color0,
    Color1,
    Color2,
    Color3,
    Color4,
    Color5,
    Color6,
    Color7,
    Depth,
    Stencil,
    DepthStencil,
}

impl From<pgfx_attachment_t> for pgfx::common::Attachment {
    fn from(value: pgfx_attachment_t) -> Self {
        match value {
            pgfx_attachment_t::Color0 => Self::Color0,
            pgfx_attachment_t::Color1 => Self::Color1,
            pgfx_attachment_t::Color2 => Self::Color2,
            pgfx_attachment_t::Color3 => Self::Color3,
            pgfx_attachment_t::Color4 => Self::Color4,
            pgfx_attachment_t::Color5 => Self::Color5,
            pgfx_attachment_t::Color6 => Self::Color6,
            pgfx_attachment_t::Color7 => Self::Color7,
            pgfx_attachment_t::Depth => Self::Depth,
            pgfx_attachment_t::Stencil => Self::Stencil,
            pgfx_attachment_t::DepthStencil => Self::DepthStencil,
        }
    }
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub enum pgfx_clear_value_type_t {
    None,
    F32,
    I32,
    U32,
    Vec4,
    F32I32,
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct pgfx_clear_value_f32i32_t {
    float32: f32,
    int32: i32,
}

#[repr(C)]
#[derive(Copy, Clone)]
pub union pgfx_clear_value_union_t {
    pub float32: f32,
    pub int32: i32,
    pub uint32: u32,
    pub vec4: [f32; 4],
    pub float32_int32: pgfx_clear_value_f32i32_t,
}

#[repr(C)]
#[derive(Copy, Clone)]
pub struct pgfx_clear_value_t {
    pub ty: pgfx_clear_value_type_t,
    pub union: pgfx_clear_value_union_t,
}

impl From<pgfx_clear_value_t> for pgfx::common::ClearValue {
    fn from(value: pgfx_clear_value_t) -> Self {
        unsafe {
            match value.ty {
                pgfx_clear_value_type_t::None => Self::None,
                pgfx_clear_value_type_t::F32 => Self::F32(value.union.float32),
                pgfx_clear_value_type_t::I32 => Self::I32(value.union.int32),
                pgfx_clear_value_type_t::U32 => Self::U32(value.union.uint32),
                pgfx_clear_value_type_t::Vec4 => Self::Vec4(value.union.vec4),
                pgfx_clear_value_type_t::F32I32 => Self::F32I32(
                    value.union.float32_int32.float32,
                    value.union.float32_int32.int32,
                ),
            }
        }
    }
}

// vertex/index //

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub enum pgfx_index_type_t {
    U16,
    U32,
}

impl From<pgfx_index_type_t> for pgfx::common::IndexType {
    fn from(value: pgfx_index_type_t) -> Self {
        match value {
            pgfx_index_type_t::U16 => Self::U16,
            pgfx_index_type_t::U32 => Self::U32,
        }
    }
}

#[repr(u8)]
#[derive(Debug, Copy, Clone, Default)]
pub enum pgfx_attribute_type_t {
    #[default]
    None,
    U8,
    F32,
    I32,
    U32,
}

impl From<pgfx_attribute_type_t> for pgfx::common::AttributeType {
    fn from(value: pgfx_attribute_type_t) -> Self {
        match value {
            pgfx_attribute_type_t::None => Self::None,
            pgfx_attribute_type_t::U8 => Self::U8,
            pgfx_attribute_type_t::F32 => Self::F32,
            pgfx_attribute_type_t::I32 => Self::I32,
            pgfx_attribute_type_t::U32 => Self::U32,
        }
    }
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct pgfx_attribute_t {
    pub location: u8,
    pub ty: pgfx_attribute_type_t,
    pub num: u8,
    pub normalize: bool,
}

impl From<pgfx_attribute_t> for pgfx::common::Attribute {
    fn from(value: pgfx_attribute_t) -> Self {
        Self {
            location: value.location,
            ty: value.ty.into(),
            num: value.num,
            normalize: value.normalize,
        }
    }
}

// texture //

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub enum pgfx_format_t {
    R8,
    RGB8,
    RGBA8,
    RGB16F,
    RGBA16F,
    RGB32F,
    RGBA32F,
    Depth32,
    Depth32Stencil8,
}

impl From<pgfx_format_t> for pgfx::common::Format {
    fn from(value: pgfx_format_t) -> Self {
        match value {
            pgfx_format_t::R8 => Self::R8,
            pgfx_format_t::RGB8 => Self::RGB8,
            pgfx_format_t::RGBA8 => Self::RGBA8,
            pgfx_format_t::RGB16F => Self::RGB16F,
            pgfx_format_t::RGBA16F => Self::RGBA16F,
            pgfx_format_t::RGB32F => Self::RGB32F,
            pgfx_format_t::RGBA32F => Self::RGBA32F,
            pgfx_format_t::Depth32 => Self::Depth32,
            pgfx_format_t::Depth32Stencil8 => Self::Depth32Stencil8,
        }
    }
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub enum pgfx_filter_t {
    Nearest = 0,
    Linear = 1,
}

impl From<pgfx_filter_t> for pgfx::common::Filter {
    fn from(value: pgfx_filter_t) -> Self {
        match value {
            pgfx_filter_t::Nearest => Self::Nearest,
            pgfx_filter_t::Linear => Self::Linear,
        }
    }
}

impl From<pgfx::common::Filter> for pgfx_filter_t {
    fn from(value: pgfx::common::Filter) -> Self {
        match value {
            pgfx::common::Filter::Nearest => Self::Nearest,
            pgfx::common::Filter::Linear => Self::Linear,
        }
    }
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub enum pgfx_wrap_t {
    Repeat = 0,
    MirroredRepeat = 1,
    Clamp = 2,
}

impl From<pgfx_wrap_t> for pgfx::common::Wrap {
    fn from(value: pgfx_wrap_t) -> Self {
        match value {
            pgfx_wrap_t::Repeat => Self::Repeat,
            pgfx_wrap_t::MirroredRepeat => Self::MirroredRepeat,
            pgfx_wrap_t::Clamp => Self::Clamp,
        }
    }
}

impl From<pgfx::common::Wrap> for pgfx_wrap_t {
    fn from(value: pgfx::common::Wrap) -> Self {
        match value {
            pgfx::common::Wrap::Repeat => Self::Repeat,
            pgfx::common::Wrap::MirroredRepeat => Self::MirroredRepeat,
            pgfx::common::Wrap::Clamp => Self::Clamp,
        }
    }
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub enum pgfx_anisotropic_filter_t {
    None = 0,
    X4 = 1,
    X8 = 2,
    X16 = 3,
}

impl From<pgfx_anisotropic_filter_t> for pgfx::common::AnisotropicFilter {
    fn from(value: pgfx_anisotropic_filter_t) -> Self {
        match value {
            pgfx_anisotropic_filter_t::None => Self::None,
            pgfx_anisotropic_filter_t::X4 => Self::X4,
            pgfx_anisotropic_filter_t::X8 => Self::X8,
            pgfx_anisotropic_filter_t::X16 => Self::X16,
        }
    }
}

impl From<pgfx::common::AnisotropicFilter> for pgfx_anisotropic_filter_t {
    fn from(value: pgfx::common::AnisotropicFilter) -> Self {
        match value {
            pgfx::common::AnisotropicFilter::None => Self::None,
            pgfx::common::AnisotropicFilter::X4 => Self::X4,
            pgfx::common::AnisotropicFilter::X8 => Self::X8,
            pgfx::common::AnisotropicFilter::X16 => Self::X16,
        }
    }
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub enum pgfx_texture_type_t {
    _2D,
}

impl From<pgfx_texture_type_t> for pgfx::common::TextureType {
    fn from(value: pgfx_texture_type_t) -> Self {
        match value {
            pgfx_texture_type_t::_2D => Self::_2D,
        }
    }
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct pgfx_sampler_t {
    bits: u16,
}

impl From<pgfx_sampler_t> for pgfx::common::Sampler {
    fn from(value: pgfx_sampler_t) -> Self {
        unsafe { std::mem::transmute(value) }
    }
}

impl From<pgfx::common::Sampler> for pgfx_sampler_t {
    fn from(value: pgfx::common::Sampler) -> Self {
        assert!(
            std::mem::size_of::<pgfx_sampler_t>() == std::mem::size_of::<pgfx::common::Sampler>()
        );
        unsafe { std::mem::transmute(value) }
    }
}

#[no_mangle]
pub extern "C" fn pgfx_sampler_init() -> pgfx_sampler_t {
    pgfx::common::Sampler::new().into()
}

#[no_mangle]
pub extern "C" fn pgfx_sampler_set_filter(sampler: *mut pgfx_sampler_t, filter: pgfx_filter_t) {
    pgfx_sampler_set_min_filter(sampler, filter);
    pgfx_sampler_set_mag_filter(sampler, filter);
}

#[no_mangle]
pub extern "C" fn pgfx_sampler_set_min_filter(sampler: *mut pgfx_sampler_t, filter: pgfx_filter_t) {
    let mut s: pgfx::common::Sampler = unsafe { (*sampler).into() };
    s.set_min_filter(filter.into());
    unsafe { *sampler = s.into() }
}

#[no_mangle]
pub extern "C" fn pgfx_sampler_set_mag_filter(sampler: *mut pgfx_sampler_t, filter: pgfx_filter_t) {
    let mut s: pgfx::common::Sampler = unsafe { (*sampler).into() };
    s.set_mag_filter(filter.into());
    unsafe { *sampler = s.into() }
}

#[no_mangle]
pub extern "C" fn pgfx_sampler_set_anisotropic_filter(
    sampler: *mut pgfx_sampler_t,
    filter: pgfx_anisotropic_filter_t,
) {
    let mut s: pgfx::common::Sampler = unsafe { (*sampler).into() };
    s.set_anisotropic_filter(filter.into());
    unsafe { *sampler = s.into() }
}

#[no_mangle]
pub extern "C" fn pgfx_sampler_set_wrap(sampler: *mut pgfx_sampler_t, wrap: pgfx_wrap_t) {
    pgfx_sampler_set_wrap_u(sampler, wrap);
    pgfx_sampler_set_wrap_v(sampler, wrap);
    pgfx_sampler_set_wrap_w(sampler, wrap);
}

#[no_mangle]
pub extern "C" fn pgfx_sampler_set_wrap_u(sampler: *mut pgfx_sampler_t, wrap: pgfx_wrap_t) {
    let mut s: pgfx::common::Sampler = unsafe { (*sampler).into() };
    s.set_wrap_u(wrap.into());
    unsafe { *sampler = s.into() }
}

#[no_mangle]
pub extern "C" fn pgfx_sampler_set_wrap_v(sampler: *mut pgfx_sampler_t, wrap: pgfx_wrap_t) {
    let mut s: pgfx::common::Sampler = unsafe { (*sampler).into() };
    s.set_wrap_v(wrap.into());
    unsafe { *sampler = s.into() }
}

#[no_mangle]
pub extern "C" fn pgfx_sampler_set_wrap_w(sampler: *mut pgfx_sampler_t, wrap: pgfx_wrap_t) {
    let mut s: pgfx::common::Sampler = unsafe { (*sampler).into() };
    s.set_wrap_w(wrap.into());
    unsafe { *sampler = s.into() }
}

#[no_mangle]
pub extern "C" fn pgfx_sampler_get_hash(sampler: *const pgfx_sampler_t) -> u16 {
    let s: pgfx::common::Sampler = unsafe { (*sampler).into() };
    s.get_hash()
}

#[no_mangle]
pub extern "C" fn pgfx_sampler_get_min_filter(sampler: *const pgfx_sampler_t) -> pgfx_filter_t {
    let s: pgfx::common::Sampler = unsafe { (*sampler).into() };
    s.get_min_filter().into()
}

#[no_mangle]
pub extern "C" fn pgfx_sampler_get_mag_filter(sampler: *const pgfx_sampler_t) -> pgfx_filter_t {
    let s: pgfx::common::Sampler = unsafe { (*sampler).into() };
    s.get_mag_filter().into()
}

#[no_mangle]
pub extern "C" fn pgfx_sampler_get_anisotropic_filter(
    sampler: *const pgfx_sampler_t,
) -> pgfx_anisotropic_filter_t {
    let s: pgfx::common::Sampler = unsafe { (*sampler).into() };
    s.get_anisotropic_filter().into()
}

#[no_mangle]
pub extern "C" fn pgfx_sampler_get_wrap_u(sampler: *const pgfx_sampler_t) -> pgfx_wrap_t {
    let s: pgfx::common::Sampler = unsafe { (*sampler).into() };
    s.get_wrap_u().into()
}

#[no_mangle]
pub extern "C" fn pgfx_sampler_get_wrap_v(sampler: *const pgfx_sampler_t) -> pgfx_wrap_t {
    let s: pgfx::common::Sampler = unsafe { (*sampler).into() };
    s.get_wrap_v().into()
}

#[no_mangle]
pub extern "C" fn pgfx_sampler_get_wrap_w(sampler: *const pgfx_sampler_t) -> pgfx_wrap_t {
    let s: pgfx::common::Sampler = unsafe { (*sampler).into() };
    s.get_wrap_w().into()
}

#[repr(C)]
#[derive(Debug, Clone, Copy)]
pub struct pgfx_blend_state_t {
    pub func: pgfx_blend_func_t,
    pub equation: pgfx_blend_equation_t,
}

impl From<pgfx_blend_state_t> for pgfx::common::BlendState {
    fn from(value: pgfx_blend_state_t) -> Self {
        Self {
            func: value.func.into(),
            equation: value.equation.into(),
        }
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy)]
pub struct pgfx_blend_func_t {
    pub source: pgfx_blend_factor_t,
    pub destination: pgfx_blend_factor_t,
}

impl From<pgfx_blend_func_t> for pgfx::common::BlendFunc {
    fn from(value: pgfx_blend_func_t) -> Self {
        Self {
            source: value.source.into(),
            destination: value.destination.into(),
        }
    }
}

#[no_mangle]
pub extern "C" fn pgfx_blend_func_default() -> pgfx_blend_func_t {
    pgfx_blend_func_t {
        source: pgfx_blend_factor_t::One,
        destination: pgfx_blend_factor_t::Zero,
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy)]
pub enum pgfx_blend_factor_t {
    Zero,
    One,
    SrcColor,
    OneMinusSrcColor,
    DstColor,
    OneMinusDstColor,
    SrcAlpha,
    OneMinusSrcAlpha,
    DstAlpha,
    OneMinusDstAlpha,
    ConstantColor,
    OneMinusConstantColor,
    ConstantAlpha,
    OneMinusConstantAlpha,
    SrcAlphaSaturate,
    Src1Color,
    OneMinusSrc1Color,
    // 	src1Alpha,
    OneMinusSrc1Alpha,
}

impl From<pgfx_blend_factor_t> for pgfx::common::BlendFactor {
    fn from(value: pgfx_blend_factor_t) -> Self {
        match value {
            pgfx_blend_factor_t::Zero => Self::Zero,
            pgfx_blend_factor_t::One => Self::One,
            pgfx_blend_factor_t::SrcColor => Self::SrcColor,
            pgfx_blend_factor_t::OneMinusSrcColor => Self::OneMinusSrcColor,
            pgfx_blend_factor_t::DstColor => Self::DstColor,
            pgfx_blend_factor_t::OneMinusDstColor => Self::OneMinusDstColor,
            pgfx_blend_factor_t::SrcAlpha => Self::SrcAlpha,
            pgfx_blend_factor_t::OneMinusSrcAlpha => Self::OneMinusSrcAlpha,
            pgfx_blend_factor_t::DstAlpha => Self::DstAlpha,
            pgfx_blend_factor_t::OneMinusDstAlpha => Self::OneMinusDstAlpha,
            pgfx_blend_factor_t::ConstantColor => Self::ConstantColor,
            pgfx_blend_factor_t::OneMinusConstantColor => Self::OneMinusConstantColor,
            pgfx_blend_factor_t::ConstantAlpha => Self::ConstantAlpha,
            pgfx_blend_factor_t::OneMinusConstantAlpha => Self::OneMinusConstantAlpha,
            pgfx_blend_factor_t::SrcAlphaSaturate => Self::SrcAlphaSaturate,
            pgfx_blend_factor_t::Src1Color => Self::Src1Color,
            pgfx_blend_factor_t::OneMinusSrc1Color => Self::OneMinusSrc1Color,
            pgfx_blend_factor_t::OneMinusSrc1Alpha => Self::OneMinusSrc1Alpha,
        }
    }
}

#[derive(PartialEq, Eq, Debug, Clone, Copy)]
pub enum pgfx_blend_equation_t {
    Add,
    Subtract,
    ReverseSubtract,
    Min,
    Max,
}

impl From<pgfx_blend_equation_t> for pgfx::common::BlendEquation {
    fn from(value: pgfx_blend_equation_t) -> Self {
        match value {
            pgfx_blend_equation_t::Add => Self::Add,
            pgfx_blend_equation_t::Subtract => Self::Subtract,
            pgfx_blend_equation_t::ReverseSubtract => Self::ReverseSubtract,
            pgfx_blend_equation_t::Min => Self::Min,
            pgfx_blend_equation_t::Max => Self::Max,
        }
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy)]
pub struct pgfx_depth_state_t {
    pub func: pgfx_depth_func_t,
}

impl From<pgfx_depth_state_t> for pgfx::common::DepthState {
    fn from(value: pgfx_depth_state_t) -> Self {
        Self {
            func: value.func.into(),
        }
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy)]
pub enum pgfx_depth_func_t {
    Never,
    Less,
    Equal,
    LEqual,
    Greater,
    NotEqual,
    GEqual,
    Always,
}

impl From<pgfx_depth_func_t> for pgfx::common::DepthFunc {
    fn from(value: pgfx_depth_func_t) -> Self {
        match value {
            pgfx_depth_func_t::Never => Self::Never,
            pgfx_depth_func_t::Less => Self::Less,
            pgfx_depth_func_t::Equal => Self::Equal,
            pgfx_depth_func_t::LEqual => Self::LEqual,
            pgfx_depth_func_t::Greater => Self::Greater,
            pgfx_depth_func_t::NotEqual => Self::NotEqual,
            pgfx_depth_func_t::GEqual => Self::GEqual,
            pgfx_depth_func_t::Always => Self::Always,
        }
    }
}

// TODO add depth_func default (LESS)


#[repr(C)]
#[derive(Debug, Clone, Copy)]
pub struct pgfx_cull_state_t {
    pub mode: pgfx_cull_mode_t,
    pub front_face: pgfx_cull_front_face_t,
}

impl From<pgfx_cull_state_t> for pgfx::common::CullState {
    fn from(value: pgfx_cull_state_t) -> Self {
        Self { mode: value.mode.into(), front_face: value.front_face.into() }
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy)]
pub enum pgfx_cull_mode_t {
    Front,
    Back,
    FrontAndBack,
}

impl From<pgfx_cull_mode_t> for pgfx::common::CullMode {
    fn from(value: pgfx_cull_mode_t) -> Self {
        match value {
            pgfx_cull_mode_t::Front => Self::Front,
            pgfx_cull_mode_t::Back => Self::Back,
            pgfx_cull_mode_t::FrontAndBack => Self::FrontAndBack,
        }
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy)]
pub enum pgfx_cull_front_face_t {
    Cw,
    Ccw,
}

impl From<pgfx_cull_front_face_t> for pgfx::common::CullFrontFace {
    fn from(value: pgfx_cull_front_face_t) -> Self {
        match value {
            pgfx_cull_front_face_t::Cw => Self::Cw,
            pgfx_cull_front_face_t::Ccw => Self::Ccw,
        }
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy)]
pub struct pgfx_scissor_state_t {
    pub rect: [i32; 4],
}

impl From<pgfx_scissor_state_t> for pgfx::common::ScissorState {
    fn from(value: pgfx_scissor_state_t) -> Self {
        Self { rect: value.rect }
    }
}

// utils //
#[repr(C)]
#[derive(Debug, Clone)]
pub struct TypedSlice<T> {
    payload: *mut T,
    length: usize,
}

impl<T> From<TypedSlice<T>> for &[T] {
    fn from(value: TypedSlice<T>) -> Self {
        unsafe { std::slice::from_raw_parts(value.payload, value.length) }
    }
}

pub type pgfx_u8_slice_t = TypedSlice<u8>;

#[repr(C)]
#[derive(Copy, Clone)]
pub struct pgfx_att_tex_cv_t {
    att: pgfx_attachment_t,
    tex: pgfx_texture_handle_t,
    cv: pgfx_clear_value_t,
}

impl From<pgfx_att_tex_cv_t>
    for (
        pgfx::common::Attachment,
        pgfx::common::TextureHandle,
        pgfx::common::ClearValue,
    )
{
    fn from(value: pgfx_att_tex_cv_t) -> Self {
        (value.att.into(), value.tex.into(), value.cv.into())
    }
}

pub type pgfx_att_tex_cv_slice_t = TypedSlice<pgfx_att_tex_cv_t>;

pub type pgfx_attribute_slice_t = TypedSlice<pgfx_attribute_t>;
