#![allow(non_camel_case_types)]

use pgfx::GfxApi;
use tracing::{error, debug};

mod helper;
pub mod types;

/// gfx functions ///

// init/destroy //
#[no_mangle]
pub extern "C" fn pgfx_gfx_create(
    platform_handle: types::pgfx_platform_handle_t,
) -> types::pgfx_gfx_t {
    pgfx_gfx_create_with_config(platform_handle.into(), types::pgfx_config_init())
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_create_with_config(
    platform_handle: types::pgfx_platform_handle_t,
    config: types::pgfx_config_t,
) -> types::pgfx_gfx_t {
    let gfx = match pgfx::Gfx::new_with_config(platform_handle.into(), config.into()) {
        Ok(gfx) => gfx,
        Err(err) => {
            error!("Failed to create Gfx instance: {:?}", err);
            return std::ptr::null_mut();
        }
    };
    let gfx = Box::new(gfx);
    let ptr = Box::into_raw(gfx);
    ptr as types::pgfx_gfx_t
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_destroy(gfx: types::pgfx_gfx_t) {
    pgfx_gfx_drop!(gfx)
}

#[no_mangle]
pub extern "C" fn pgfx_init_logging() {
    tracing_subscriber::fmt::init();
}

// context resize //
#[no_mangle]
pub extern "C" fn pgfx_gfx_resize_window(gfx: types::pgfx_gfx_t, size_x: u32, size_y: u32) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.resize_window((size_x as _, size_y as _));
    });
}

// render target //
#[no_mangle]
pub extern "C" fn pgfx_gfx_clear_render_target(
    gfx: types::pgfx_gfx_t,
    handle: types::pgfx_render_target_handle_t,
) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.clear_render_target(handle.into());
    })
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_clear_render_target_att_cv(
    gfx: types::pgfx_gfx_t,
    handle: types::pgfx_render_target_handle_t,
    att: types::pgfx_attachment_t,
    cv: types::pgfx_clear_value_t,
) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.clear_render_target_att_cv(handle.into(), att.into(), cv.into());
    });
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_create_render_target(gfx: types::pgfx_gfx_t, textures: types::pgfx_att_tex_cv_slice_t) -> types::pgfx_render_target_handle_t {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        let mapped: &[types::pgfx_att_tex_cv_t] = textures.into();
        let mapped: Vec<_> = mapped.iter().copied().map(|v| v.into()).collect();
        gfx.create_render_target(&mapped).into()
    })
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_destroy_render_target(gfx: types::pgfx_gfx_t, handle: types::pgfx_render_target_handle_t) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.destroy_render_target(handle.into());
    });
}

// draw //
#[no_mangle]
pub extern "C" fn pgfx_gfx_present(gfx: types::pgfx_gfx_t, mode: types::pgfx_present_mode_t) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.present(mode.into());
    })
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_draw(gfx: types::pgfx_gfx_t) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.draw();
    });
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_draw_instances_base(gfx: types::pgfx_gfx_t, instances: u32, base_instance: u32) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.draw_instances_base(instances, base_instance);
    });
}

// shader //
#[no_mangle]
pub extern "C" fn pgfx_gfx_create_vertex_shader(gfx: types::pgfx_gfx_t, data: types::pgfx_u8_slice_t) -> types::pgfx_vertex_shader_handle_t {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.create_vertex_shader(data.into()).into()
    })
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_destroy_vertex_shader(gfx: types::pgfx_gfx_t, handle: types::pgfx_vertex_shader_handle_t) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.destroy_vertex_shader(handle.into());
    });
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_create_fragment_shader(gfx: types::pgfx_gfx_t, data: types::pgfx_u8_slice_t) -> types::pgfx_fragment_shader_handle_t {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.create_fragment_shader(data.into()).into()
    })
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_destroy_fragment_shader(gfx: types::pgfx_gfx_t, handle: types::pgfx_fragment_shader_handle_t) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.destroy_fragment_shader(handle.into());
    });
}

// vertex/index buffer //
#[no_mangle]
pub extern "C" fn pgfx_gfx_create_vertex_buffer(gfx: types::pgfx_gfx_t, data: types::pgfx_u8_slice_t, attributes: types::pgfx_attribute_slice_t) -> types::pgfx_vertex_buffer_handle_t {
    let mapped: &[types::pgfx_attribute_t] = attributes.into();
    debug!("mapped attributes: {:?}", mapped);
    let mapped: Vec<_> = mapped.iter().copied().map(|v| v.into()).collect();
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.create_vertex_buffer(data.into(), &mapped)
    }).into()
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_destroy_vertex_buffer(gfx: types::pgfx_gfx_t, handle: types::pgfx_vertex_buffer_handle_t) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.destroy_vertex_buffer(handle.into());
    });
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_create_index_buffer(gfx: types::pgfx_gfx_t, data: types::pgfx_u8_slice_t, index_type: types::pgfx_index_type_t) -> types::pgfx_index_buffer_handle_t {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.create_index_buffer(data.into(), index_type.into())
    }).into()
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_destroy_index_buffer(gfx: types::pgfx_gfx_t, handle: types::pgfx_index_buffer_handle_t) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.destroy_index_buffer(handle.into());
    });
}


// uniform/storage buffer //
#[no_mangle]
pub extern "C" fn pgfx_gfx_create_uniform_buffer(gfx: types::pgfx_gfx_t, size: u32) -> types::pgfx_uniform_buffer_handle_t {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.create_uniform_buffer(size)
    }).into()
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_destroy_uniform_buffer(gfx: types::pgfx_gfx_t, handle: types::pgfx_uniform_buffer_handle_t) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.destroy_uniform_buffer(handle.into())
    });
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_update_uniform_buffer(gfx: types::pgfx_gfx_t, handle: types::pgfx_uniform_buffer_handle_t, data: types::pgfx_u8_slice_t) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.update_uniform_buffer(handle.into(), data.into());
    });
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_update_uniform_buffer_offset(gfx: types::pgfx_gfx_t, handle: types::pgfx_uniform_buffer_handle_t, data: types::pgfx_u8_slice_t, offset: u32) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.update_uniform_buffer_offset(handle.into(), data.into(), offset);
    });
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_create_storage_buffer(gfx: types::pgfx_gfx_t, size: u32) -> types::pgfx_storage_buffer_handle_t {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.create_storage_buffer(size)
    }).into()
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_destroy_storage_buffer(gfx: types::pgfx_gfx_t, handle: types::pgfx_storage_buffer_handle_t) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.destroy_storage_buffer(handle.into())
    });
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_update_storage_buffer(gfx: types::pgfx_gfx_t, handle: types::pgfx_storage_buffer_handle_t, data: types::pgfx_u8_slice_t) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.update_storage_buffer(handle.into(), data.into());
    });
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_update_storage_buffer_offset(gfx: types::pgfx_gfx_t, handle: types::pgfx_storage_buffer_handle_t, data: types::pgfx_u8_slice_t, offset: u32) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.update_storage_buffer_offset(handle.into(), data.into(), offset);
    });
}

// texture/sampler //
#[no_mangle]
pub extern "C" fn pgfx_gfx_destroy_texture(gfx: types::pgfx_gfx_t, handle: types::pgfx_texture_handle_t) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.destroy_texture(handle.into());
    });
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_create_texture_2d(gfx: types::pgfx_gfx_t, width: u32, height: u32, fmt: types::pgfx_format_t, mip_levels: u32) -> types::pgfx_texture_handle_t {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.create_texture_2d(width, height, fmt.into(), mip_levels)
    }).into()
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_update_texture(gfx: types::pgfx_gfx_t, handle: types::pgfx_texture_handle_t, mip_level: u32, offset_x: u32, offset_y: u32,
    offset_z: u32, width: u32, height: u32, depth: u32, data: types::pgfx_u8_slice_t) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.update_texture(handle.into(), mip_level, offset_x, offset_y, offset_z, width, height, depth, data.into());
    });
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_get_bindless_texture_handle(gfx: types::pgfx_gfx_t, tex: types::pgfx_texture_handle_t, sampler: types::pgfx_sampler_t) -> types::pgfx_bindless_texture_handle_t {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.get_bindless_texture_handle(tex.into(), sampler.into())
    }).into()
}

// draw state //
#[no_mangle]
pub extern "C" fn pgfx_gfx_set_vertex_buffer(gfx: types::pgfx_gfx_t, handle: types::pgfx_vertex_buffer_handle_t) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.set_vertex_buffer(handle.into());
    });
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_set_index_buffer(gfx: types::pgfx_gfx_t, handle: types::pgfx_index_buffer_handle_t) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.set_index_buffer(handle.into());
    });
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_set_vertex_shader(gfx: types::pgfx_gfx_t, handle: types::pgfx_vertex_shader_handle_t) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.set_vertex_shader(handle.into());
    });
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_set_fragment_shader(gfx: types::pgfx_gfx_t, handle: types::pgfx_fragment_shader_handle_t) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.set_fragment_shader(handle.into());
    });
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_set_texture(gfx: types::pgfx_gfx_t, handle: types::pgfx_texture_handle_t, location: u32) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.set_texture(handle.into(), location);
    });
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_set_sampler(gfx: types::pgfx_gfx_t, sampler: types::pgfx_sampler_t, location: u32) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.set_sampler(sampler.into(), location);
    });
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_set_texture_sampler(gfx: types::pgfx_gfx_t, tex: types::pgfx_texture_handle_t, sampler: types::pgfx_sampler_t, location: u32) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.set_texture_sampler(tex.into(), sampler.into(), location);
    });
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_set_uniform_buffer(gfx: types::pgfx_gfx_t, handle: types::pgfx_uniform_buffer_handle_t, location: u32) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.set_uniform_buffer(handle.into(), location);
    });
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_set_uniform_buffer_offset_size(gfx: types::pgfx_gfx_t, handle: types::pgfx_uniform_buffer_handle_t, location: u32, offset: u32, size: u32) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.set_uniform_buffer_offset_size(handle.into(), location, offset, size);
    });
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_set_storage_buffer(gfx: types::pgfx_gfx_t, handle: types::pgfx_storage_buffer_handle_t, location: u32) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.set_storage_buffer(handle.into(), location);
    });
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_set_storage_buffer_offset_size(gfx: types::pgfx_gfx_t, handle: types::pgfx_storage_buffer_handle_t, location: u32, offset: u32, size: u32) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.set_storage_buffer_offset_size(handle.into(), location, offset, size);
    });
}
#[no_mangle]
pub extern "C" fn pgfx_gfx_set_render_targets(gfx: types::pgfx_gfx_t, read: types::pgfx_render_target_handle_t, write: types::pgfx_render_target_handle_t) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.set_render_targets(read.into(), write.into());
    });
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_set_blend_state(gfx: types::pgfx_gfx_t, blend_state: *const types::pgfx_blend_state_t) {
    let state = match unsafe { blend_state.as_ref() } {
        Some(s) => Some((*s).into()),
        None => None,
    };
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.set_blend_state(state);
    });
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_set_depth_state(gfx: types::pgfx_gfx_t, depth_state: *const types::pgfx_depth_state_t) {
    let state = match unsafe { depth_state.as_ref() } {
        Some(s) => Some((*s).into()),
        None => None,
    };
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.set_depth_state(state);
    });
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_set_cull_state(gfx: types::pgfx_gfx_t, cull_state: *const types::pgfx_cull_state_t) {
    let state = match unsafe { cull_state.as_ref() } {
        Some(s) => Some((*s).into()),
        None => None,
    };
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.set_cull_state(state);
    });
}

#[no_mangle]
pub extern "C" fn pgfx_gfx_set_scissor_state(gfx: types::pgfx_gfx_t, scissor_state: *const types::pgfx_scissor_state_t) {
    let state = match unsafe { scissor_state.as_ref() } {
        Some(s) => Some((*s).into()),
        None => None,
    };
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.set_scissor_state(state);
    });
}

// viewport //
#[no_mangle]
pub extern "C" fn pgfx_gfx_set_viewport(gfx: types::pgfx_gfx_t, viewport: types::pgfx_viewport_t) {
    pgfx_gfx_dispatch!(gfx, |gfx: &mut pgfx::Gfx| {
        gfx.set_viewport(viewport.into());
    })
}

