#[macro_export]
macro_rules! pgfx_gfx_dispatch {
    ($gfx:expr, $func:expr) => {
        match $gfx.into() {
            Some(gfx) => {
                let gfx: &mut pgfx::Gfx = unsafe { &mut *(gfx as *mut pgfx::Gfx) };
                $func(gfx)
            }
            None => { panic!("gfx is null") }
        }
    };
}

#[macro_export]
macro_rules! pgfx_gfx_drop {
    ($gfx:expr) => {
        match $gfx.into() {
            Some(gfx) => {
                let _ = unsafe { Box::from_raw(gfx as *mut pgfx::Gfx) };
            }
            None => {}
        }
    };
}

#[macro_export]
macro_rules! pgfx_define_handle {
    ($name:ident, $id_type:ty, $rust_ty:ty) => {
        paste::paste! {
            #[repr(C)]
            #[derive(Debug, Copy, Clone, PartialEq, Eq)]
            pub struct [<pgfx_ $name _handle_t>] {
                pub id: $id_type,
            }

            impl From<$rust_ty> for [<pgfx_ $name _handle_t>] {
                fn from(value: $rust_ty) -> Self {
                    Self {
                        id: value.get_id(),
                    }
                }
            }

            impl From<[<pgfx_ $name _handle_t>]> for $rust_ty {
                fn from(value: [<pgfx_ $name _handle_t>]) -> Self {
                    Self::new(value.id)
                }
            }

            #[no_mangle]
            pub extern "C" fn [<pgfx_ $name _handle_init>]() -> [<pgfx_ $name _handle_t>] {
                $rust_ty::default().into()
            }

            #[no_mangle]
            pub extern "C" fn [<pgfx_ $name _handle_invalid>]() -> [<pgfx_ $name _handle_t>] {
                $rust_ty::invalid().into()
            }

            #[no_mangle]
            pub extern "C" fn [<pgfx_ $name _handle_is_valid>](handle: [<pgfx_ $name _handle_t>]) -> bool {
                Into::<$rust_ty>::into(handle).is_valid()
            }

            #[no_mangle]
            pub extern "C" fn [<pgfx_ $name _handle_get_id>](handle: [<pgfx_ $name _handle_t>]) -> $id_type {
                Into::<$rust_ty>::into(handle).get_id()
            }
        }
    };
}

#[macro_export]
macro_rules! pgfx_define_raw_handle {
    ($name:ident, $id_type:ty, $rust_ty:ty) => {
        paste::paste! {
            #[repr(C)]
            #[derive(Debug, Copy, Clone, PartialEq, Eq)]
            pub struct [<pgfx_ $name _handle_t>] {
                pub handle: $id_type,
            }

            impl From<$rust_ty> for [<pgfx_ $name _handle_t>] {
                fn from(value: $rust_ty) -> Self {
                    Self {
                        handle: value.get_raw_handle(),
                    }
                }
            }

            impl From<[<pgfx_ $name _handle_t>]> for $rust_ty {
                fn from(value: [<pgfx_ $name _handle_t>]) -> Self {
                    Self::new(value.handle)
                }
            }

            #[no_mangle]
            pub extern "C" fn [<pgfx_ $name _handle_init>]() -> [<pgfx_ $name _handle_t>] {
                $rust_ty::default().into()
            }

            #[no_mangle]
            pub extern "C" fn [<pgfx_ $name _handle_invalid>]() -> [<pgfx_ $name _handle_t>] {
                $rust_ty::invalid().into()
            }

            #[no_mangle]
            pub extern "C" fn [<pgfx_ $name _handle_is_valid>](handle: [<pgfx_ $name _handle_t>]) -> bool {
                Into::<$rust_ty>::into(handle).is_valid()
            }

            #[no_mangle]
            pub extern "C" fn [<pgfx_ $name _handle_get_raw_handle>](handle: [<pgfx_ $name _handle_t>]) -> $id_type {
                Into::<$rust_ty>::into(handle).get_raw_handle()
            }
        }
    };
}
