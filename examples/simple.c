#include "pgfx.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_syswm.h>
#include <stdio.h>

int main(int argc, char** argv)
{
    pgfx_init_logging();

	SDL_Init(SDL_INIT_VIDEO);
    SDL_Window* win = SDL_CreateWindow("simple", 0, 0, 1024, 768, SDL_WINDOW_SHOWN | SDL_WINDOW_ALLOW_HIGHDPI);
	if (!win) {
		printf("error creating window: %s", SDL_GetError());
	}

	SDL_SysWMinfo wm_info;
	SDL_VERSION(&wm_info.version);
	printf("major: %d, minor: %d, patch: %d\n", wm_info.version.major, wm_info.version.minor, wm_info.version.patch);
	SDL_version version;
	SDL_GetVersion(&version);
	printf("major: %d, minor: %d, patch: %d\n", version.major, version.minor, version.patch);
	if (SDL_GetWindowWMInfo(win, &wm_info) == 0) {
		printf("error getting wm_info: %s\n", SDL_GetError());
	}

	printf("default_render_target id: %hu\n", pgfx_render_target_handle_get_id(pgfx_render_target_default()));

	pgfx_platform_handle_t platform;
	switch(wm_info.subsystem) {
		case SDL_SYSWM_WAYLAND:
			platform.type = PGFX_PLATFORM_TYPE_WAYLAND;
			platform.wayland = (pgfx_platform_wayland_t) {
				.surface = wm_info.info.wl.surface,
				.display = wm_info.info.wl.display,
				.egl_window = NULL,
			};
			break;
		case SDL_SYSWM_X11:
			platform.type = PGFX_PLATFORM_TYPE_XLIB;
			platform.xlib = (pgfx_platform_xlib_t) {
				.window = wm_info.info.x11.window,
				.display = wm_info.info.x11.display,
			};
			break;
//#if _WIN32
//		case SDL_SYSWM_WINDOWS:
//			platform.type = PGFX_PLATFORM_TYPE_WIN32;
//			platform.win32 = (pgfx_platform_win32_t) {
//				.hwnd = wm_info.info.win.window,
//				.hdc = wm_info.info.win.hdc,
//				.hinstance = wm_info.info.win.hinstance,
//			};
//			break;
//#endif
		default:
			printf("no subsystem found: %d\n", wm_info.subsystem);
			return 1;
	}

	pgfx_config_t config = pgfx_config_init();
	//pgfx_platform_handle_t platform = {
	//	.type = PGFX_PLATFORM_TYPE_NONE,
	//};
	pgfx_gfx_t gfx = pgfx_gfx_create_with_config(platform, config);

	int width = 1024;
	int height = 768;
	SDL_Event evt;
	bool quit = false;
	while(!quit) {
		while(SDL_PollEvent(&evt)) {
			switch(evt.type) {
				case SDL_QUIT:
					quit = true;
					break;
				case SDL_WINDOWEVENT:
					switch(evt.window.event) {
						case SDL_WINDOWEVENT_CLOSE:
							quit = true;
							break;
						case SDL_WINDOWEVENT_RESIZED:
							int w = evt.window.data1;
							int h = evt.window.data2;
							pgfx_gfx_resize_window(gfx, w, h);
							pgfx_gfx_set_viewport(gfx, (pgfx_viewport_t) { .x = 0, .y = 0, .width = w, .height = h});
							width = w;
							height = h;
							break;
						default:
							break;
					}
					break;
				default:
					break;
			}
		}

		pgfx_gfx_clear_render_target_att_cv(gfx, pgfx_render_target_default(), PGFX_ATTACHMENT_COLOR_0, (pgfx_clear_value_t) {
			.type = PGFX_CLEAR_VALUE_VEC4,
			.vec4 = {1.0, 0.0, 0.0, 1.0}
		});
		pgfx_gfx_present(gfx, PGFX_PRESENT_MODE_VSYNC);
	}

	printf("quitting\n");

	pgfx_gfx_destroy(gfx);

	SDL_Quit();
}
