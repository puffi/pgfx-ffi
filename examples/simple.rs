use std::ffi::CString;

pub use pgfx_ffi::types::*;
pub use pgfx_ffi::*;
use sdl2_sys::sdl::{
    event::{SDL_Event, SDL_PollEvent, SDL_QUIT, SDL_WINDOWEVENT},
    syswm::{SDL_GetWindowWMInfo, SDL_SysWMinfo},
    video::{
        SDL_CreateWindow, SDL_WINDOWEVENT_CLOSE, SDL_WINDOWEVENT_RESIZED, SDL_WINDOW_ALLOW_HIGHDPI,
        SDL_WINDOW_SHOWN,
    },
    SDL_Init, SDL_Quit, SDL_bool, SDL_INIT_VIDEO,
};

fn main() {
    tracing_subscriber::fmt::init();

    unsafe { SDL_Init(SDL_INIT_VIDEO) };
    let title = CString::new("simple").unwrap();
    let win = unsafe {
        SDL_CreateWindow(
            title.as_ptr(),
            0,
            0,
            1024,
            768,
            SDL_WINDOW_SHOWN | SDL_WINDOW_ALLOW_HIGHDPI,
        )
    };
    eprintln!("window: {:X?}", win);
    assert!(!win.is_null());

    let mut wm_info: SDL_SysWMinfo = unsafe { std::mem::MaybeUninit::zeroed().assume_init() };
    wm_info.version.major = 2;
    wm_info.version.minor = 26;
    wm_info.version.patch = 5;
    assert!(unsafe { SDL_GetWindowWMInfo(win, &mut wm_info) } == SDL_bool::True);

    let platform_handle = match wm_info.subsystem {
        sdl2_sys::sdl::syswm::SDL_SYSWM_TYPE::Wayland => {
            let (surface, display, egl_window) = unsafe {
                (
                    wm_info.info.wl.surface,
                    wm_info.info.wl.display,
                    std::ptr::null_mut(),
                )
            };
            pgfx_platform_handle_t {
                ty: pgfx_platform_type_t::Wayland,
                handles: pgfx_platform_union_t {
                    wayland: pgfx_platform_wayland_t {
                        surface,
                        display,
                        egl_window,
                    },
                },
            }
        }
        sdl2_sys::sdl::syswm::SDL_SYSWM_TYPE::X11 => pgfx_platform_handle_t {
            ty: pgfx_platform_type_t::Xlib,
            handles: pgfx_platform_union_t {
                xlib: pgfx_platform_xlib_t {
                    window: unsafe { wm_info.info.x11.window },
                    display: unsafe { wm_info.info.x11.display },
                },
            },
        },
        sdl2_sys::sdl::syswm::SDL_SYSWM_TYPE::Windows => pgfx_platform_handle_t {
            ty: pgfx_platform_type_t::Win32,
            handles: pgfx_platform_union_t {
                win32: pgfx_platform_win32_t {
                    hwnd: unsafe { wm_info.info.win.window },
                    hdc: unsafe { wm_info.info.win.hdc },
                    hinstance: unsafe { wm_info.info.win.hinstance },
                },
            },
        },
        _ => unimplemented!(),
    };
    let config = pgfx_config_init();
    //let platform_handle = pgfx_platform_handle_t {
    //    ty: pgfx_platform_type_t::Xlib,
    //    handles: pgfx_platform_union_t {
    //        xlib: pgfx_platform_xlib_t {
    //            window: unsafe { wm_info.info.x11.window },
    //            display: unsafe { wm_info.info.x11.display },
    //        },
    //    },
    //};
    let gfx = pgfx_gfx_create_with_config(platform_handle, config);

    let mut evt = SDL_Event::default();
    let mut quit = false;
    while !quit {
        while unsafe { SDL_PollEvent(&mut evt) } != 0 {
            match unsafe { evt.ty } {
                SDL_QUIT => quit = true,
                SDL_WINDOWEVENT => match unsafe { evt.window.event } {
                    SDL_WINDOWEVENT_CLOSE => quit = true,
                    SDL_WINDOWEVENT_RESIZED => {
                        let (w, h) = unsafe { (evt.window.data1, evt.window.data2) };
                        eprintln!("new size: {}x{}", w, h);
                        pgfx_gfx_resize_window(gfx, w as u32, h as u32);
                        pgfx_gfx_set_viewport(
                            gfx,
                            pgfx_viewport_t {
                                x: 0,
                                y: 0,
                                width: w,
                                height: h,
                            },
                        );
                    }
                    _ => {}
                },
                _ => {}
            }
        }

        //pgfx_gfx_clear_render_target(gfx, DEFAULT_RENDER_TARGET);
        pgfx_gfx_clear_render_target_att_cv(
            gfx,
            pgfx_render_target_default(),
            pgfx_attachment_t::Color0,
            pgfx_clear_value_t {
                ty: pgfx_clear_value_type_t::Vec4,
                union: pgfx_clear_value_union_t {
                    vec4: [1.0, 0.0, 0.0, 1.0],
                },
            },
        );
        pgfx_gfx_present(gfx, pgfx_present_mode_t::Vsync);
    }

    pgfx_gfx_destroy(gfx);

    unsafe { SDL_Quit() };
}
